const express = require('express');
const bodyParser = require('body-parser');
var cors = require('cors');
const app = express();

app.use(bodyParser.json());

app.use(function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header("Access-Control-Allow-Headers", "*");
    res.header('Access-Control-Allow-Credentials', true);
    res.header('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE, OPTIONS');
    next();
});

const pool = require('./db').pool;

//Wo wird die Verbindung zur DB aufgebaut?

// Anlegen einer Anzeige, User-Story 1, Sprint-Backlog ID 006
// app.post("/api/entry", cors(), async function (req, res) {
app.post("/api/entry", async function (req, res) {
    const anzeige = req.body;
    let conn;
    try {
        conn = await pool.getConnection();
        await conn.query(
            "INSERT INTO anzeige (titel, beschreibung, vorname, nachname, ort, preis, verhandlungsbasis, kontakt) VALUES (?, ?, ?, ?, ?, ?, ?, ?)",
            [anzeige.titel, anzeige.beschreibung, anzeige.vorname, anzeige.nachname, anzeige.ort, anzeige.preis, anzeige.verhandlungsbasis, anzeige.kontakt]
        );
        res.status(201).end();
    }
    catch (err) {
        console.error(err);
        res.status(500).end();
    } finally {
        if (conn) conn.release(); //release to pool
    }
});

// Liste von Anzeigen, User-Story 2, Sprint-Backlog 007
app.get("/api/entry", cors(), async function (req, res) {
    let conn;
    try {
        conn = await pool.getConnection();
        const result = await conn.query("SELECT `titel`, LEFT (beschreibung, 100), `preis`, `ort`, `create`, `anzeigeId` FROM `anzeige` WHERE `create` > DATE_SUB( NOW(), INTERVAL 14 DAY ) ORDER BY `anzeigeId` DESC LIMIT 20");
        res.json(result); 
    } 
    catch (err) {
        console.error(err);
        res.status(500).end();
    } finally {
        if (conn) conn.release(); //release to pool
    }
});

// Abfrage einzelne Anzeige, User-Story 3, Sprint-Backlog ID 008
app.get('/api/entry/:id', async function (req, res) {
    const anzeigeId = req.params.id;
    let conn;
    try {
        conn = await pool.getConnection();
        const result = await conn.query("SELECT * FROM `anzeige` WHERE anzeigeId = ?", [anzeigeId]);
        res.json(result);
    } catch (err) {
        console.log(err);
        throw err;
    } finally {
        if (conn) conn.release(); //release to pool
    }
});

//Abfrage Ort, User-Story 4, Sprint-Backlog ID 009
app.get('/api/ort/:ort', async function (req, res) {
    const ort = decodeURI(req.params.ort); //decodeURI dekodiert die für die URI verschlüsselten Sonderzeichen
    let conn;
    try {
        conn = await pool.getConnection();
        const result = await conn.query("SELECT `titel`, LEFT (beschreibung, 100), `preis`, `ort`, `create`, `anzeigeId` FROM `anzeige` WHERE `ort` = ? AND `create` > DATE_SUB( NOW(), INTERVAL 14 DAY ) ORDER BY `anzeigeId` DESC LIMIT 20", [ort]);
        res.json(result);
    } catch (err) {
        console.log(err);
        throw err;
    } finally {
        if (conn) conn.release(); //release to pool
    }
});

//Abfrage Suchbegriff, User-Story 5, Sprint-Backlog ID 010
app.get('/api/keyword/:keyword', async function (req, res) {
    const keyword = req.params.keyword;
    let conn;
    try {
        conn = await pool.getConnection();
        // const result = await conn.query("SELECT `titel`, `beschreibung` FROM `anzeige` WHERE CONTAINS(beschreibung, ?) ORDER BY `anzeigeId` DESC LIMIT 20", [keyword]);
        const result = await conn.query("SELECT `titel`, `beschreibung` FROM `anzeige` WHERE `beschreibung` LIKE ? AND `create` > DATE_SUB( NOW(), INTERVAL 14 DAY ) ORDER BY `anzeigeId` DESC LIMIT 20", ['%' + keyword + '%']);
        res.json(result);
    } catch (err) {
        console.log(err);
        throw err;
    } finally {
        if (conn) conn.release(); //release to pool
    }
});

//Abfrage beliebtester Ort, User-Story 6, Sprint-Backlog ID 011
app.get('/api/most_wanted_ort/', async function (req, res) {
    let conn;
    try {
        conn = await pool.getConnection();
        const result = await conn.query("SELECT `ort`, `create`, COUNT(`ort`) AS `value_occurrence` FROM `anzeige` WHERE `create` > DATE_SUB( NOW(), INTERVAL 14 DAY ) GROUP BY `ort` ORDER BY `value_occurrence` DESC LIMIT 5;");

        res.json(result);
    } catch (err) {
        console.log(err);
        throw err;
    } finally {
        if (conn) conn.release(); //release to pool
    }
});

//Abfrage Suchbegriff, User-Story 7, Sprint-Backlog ID 012
// Keine zusaetzliche Funktion...

app.use(express.static('public'));
app.use('/jquery', express.static('node_modules/jquery/dist'));

app.listen(3000, function () {
    console.log("listening on http://127.0.0.1:3000");
});
