function viewAnzeigeListe() {
  $('#seiteninhalt').load('anzeigeliste.html', function () {
    $('#buttonNachOrtFiltern').click(nachOrtFiltern);
    $('#buttonFilterEntfernen').click(viewAnzeigeListe);
    $('#buttonNachKeywordFiltern').click(nachKeywordFiltern);
    neuesteAnzeigenAusfuellen();
    datenFavouritesHolen();

    $('select').change(function () {
      let ortsauswahl = $('#selectOrt').val();
      nachFavouriteFiltern(ortsauswahl);
    });
  });
}

function neuesteAnzeigenAusfuellen() {
  fetch('http://127.0.0.1:3000/api/entry')
    .then(response => response.json())
    .then(data => {
      $('table').empty(); //Leert eine eventuell bestehende Tabelle, um Doppeleinträge zu vermeiden

      console.log(data)
      let table = document.querySelector("table");
      let keys = Object.keys(data[0]);
      generateTable(table, data);
      generateTableHead(table, keys);
      addEventListenerAnzeigen();
    })
    .catch(err => {
      console.log(err);
      // Do something for an error here
    })
}

function generateTableHead(table, data) {
  const headings = ['Angebot', 'Beschreibung', 'Preis', 'Ort', 'Erstellt am', 'Anzeigen-Nr.'];

  let thead = table.createTHead();
  let row = thead.insertRow();
  let th = document.createElement("th");
  row.appendChild(th);

  for (let key in data) {
    let th = document.createElement("th");
    let text = document.createTextNode(headings[key]);
    th.appendChild(text);
    row.appendChild(th);
  }
}

function generateTable(table, data) {
  let counter = 1;

  for (let element of data) {
    let row = table.insertRow();
    let rowNumberCell = row.insertCell();
    row.classList.add('tabellenlink');
    let rowNumbertext = document.createTextNode(counter);
    rowNumberCell.appendChild(rowNumbertext);

    row.id = element.anzeigeId;
    for (key in element) {
      let text = document.createElement('div');
      let cell = row.insertCell();

      if (key == 'LEFT (beschreibung, 100)') {
        if (element[key].length == 100) {
          element[key] += '&hellip;'; //Füge ein Auslassungszeichen (...) an, wenn die Beschreibung abgeschnitten werden musste.
        }
        text.innerHTML = element[key];
      }

      else if (key == 'preis') {
        let preis = element[key].toLocaleString('de-DE', 'currency'); //fügt ein Trennzeichen nach dem Tausender ein und sorgt für den korrekten Dezimaltrenner
        if (preis.indexOf(',') > 0) {
          text.innerHTML = '<div class="has-text-right">' + preis + '&nbsp;&euro;</div>'
        } else {
          text.innerHTML = '<div class="has-text-right" id="preisSpalte">' + preis + ',' + '&ndash;&nbsp;&euro;</div>';
        }
      }

      else if (key == 'create') {
        //lese das Erstellungsdatum aus
        const date = sqlToJsDate(element[key]);
        let day = date.getDate();
        if (day < 10) {
          day = '0' + day;
        }
        let month = date.getMonth();
        if (month < 10) {
          month = '0' + month;
        }
        const year = date.getFullYear();
        text.innerHTML = `${day}.${month}.${year}`;
      }

      else {
        text.innerHTML = element[key];
      }
      text.wholeText = text;
      cell.appendChild(text);
    }

    //   else if (key == 'preis') {
    //     text.innerHTML = element[key] + '&nbsp;&euro;';
    //   }

    //   else if (key == 'create') {
    //     //lese das Erstellungsdatum aus
    //     const date = sqlToJsDate(element[key]);
    //     let day = date.getDate();
    //     if (day < 10) {
    //       day = '0' + day;
    //     }
    //     let month = date.getMonth();
    //     if (month < 10) {
    //       month = '0' + month;
    //     }
    //     const year = date.getFullYear();
    //     text.innerHTML = `${day}.${month}.${year}`;
    //   }

    //   else {
    //     text.innerHTML = element[key];
    //   }
    //   text.wholeText = text;
    //   cell.appendChild(text);
    // }
    counter++;
  }
}

//Mit kleiner Korrektur von folgender Seite übernommen: https://deepinthecode.com/2014/08/05/converting-a-sql-datetime-to-a-javascript-date/
function sqlToJsDate(sqlDate) {
  //sqlDate in SQL DATETIME format ("yyyy-mm-dd hh:mm:ss.ms")
  var sqlDateArr1 = sqlDate.split("-");
  //format of sqlDateArr1[] = ['yyyy','mm','dd hh:mm:ms']
  var sYear = sqlDateArr1[0];
  var sMonth = (Number(sqlDateArr1[1])).toString();
  var sqlDateArr2 = sqlDateArr1[2].split("T");
  //format of sqlDateArr2[] = ['dd', 'hh:mm:ss.ms']
  var sDay = sqlDateArr2[0];
  var sqlDateArr3 = sqlDateArr2[1].split(":");
  //format of sqlDateArr3[] = ['hh','mm','ss.ms']
  var sHour = sqlDateArr3[0];
  var sMinute = sqlDateArr3[1];
  var sqlDateArr4 = sqlDateArr3[2].split(".");
  //format of sqlDateArr4[] = ['ss','ms']
  var sSecond = sqlDateArr4[0];

  return new Date(sYear, sMonth, sDay, sHour, sMinute, sSecond);
}

function clickHandlerAnzDetail() {
  let anzId = this.id;
  $('#seiteninhalt').load('anzeigedetail.html', function () {
    anzeigenDetailFuellen(anzId);
  });
}

function anzeigenDetailFuellen(anzId) {
  $('body').off('click', 'zurueckZuAktuelleAnzeige')
  fetch(`http://127.0.0.1:3000/api/entry/${anzId}`)
    .then(response => response.json())
    .then(data => {
      console.log(data);
      document.getElementById("Titel").innerHTML = data[0].titel;
      document.getElementById("Ort").innerHTML = data[0].ort;
      document.getElementById("Beschreibung").innerHTML = data[0].beschreibung;
      if (data[0].preis.toLocaleString('de-DE', 'currency').indexOf(',') > 0) {
        document.getElementById("Preis").innerHTML = data[0].preis.toLocaleString('de-DE', 'currency') + '&nbsp;&euro;'
      } else {
        document.getElementById("Preis").innerHTML = data[0].preis + ',' + '&ndash;&nbsp;&euro;'
      }
      if (data[0].verhandlungsbasis == 0) {
        document.getElementById("Verhandlungsbasis").innerHTML = "Preis nicht verhandelbar"
      } else {
        document.getElementById("Verhandlungsbasis").innerHTML = "Preis verhandelbar"
      }
      document.getElementById("Name").innerHTML = data[0].vorname + ' ' + data[0].nachname;
      document.getElementById("Kontakt").innerHTML = data[0].kontakt;
    });
    $('.zurueckZuAktuelleAnzeige').click(zurueckZuAnzeige);
}

function zurueckZuAnzeige (){
  viewAnzeigeListe();
}

function addEventListenerAnzeigen() {
  document.querySelectorAll('tr')
    .forEach(e => e.addEventListener("click", clickHandlerAnzDetail));
}

function datenFavouritesHolen() {
  fetch('http://127.0.0.1:3000/api/most_wanted_ort')
    .then(response => response.json())
    .then(data => {
      console.log(data)
      let orte = [];
      for (element of data) {
        orte.push(element.ort);
      }
      let select = document.getElementById("selectOrt");
      createDropDownFavourites(select, orte);

    })
    .catch(err => {
      console.log(err);
      // Do something for an error here
    })
}

function createDropDownFavourites(select, orte) {
  // Courtesy http://jsfiddle.net/yYW89/
  for (var i = 0; i < orte.length; i++) {
    var opt = orte[i];
    var el = document.createElement("option");
    el.textContent = opt;
    el.value = opt;
    select.appendChild(el);
  }
}

function nachOrtFiltern() {
  const ort = $('#ortInput').val();
  const ortKodierteSonderzeichen = encodeURI(ort);
  let art = 'Ort';

  $.get(`http://127.0.0.1:3000/api/ort/${ortKodierteSonderzeichen}`, function (tabellenEintraege, status) {
    nachOrtFuellen(tabellenEintraege, status, art);
  });

  filterEntfernenButton();
}

function nachKeywordFiltern() {
  const keyword = $('#keywordInput').val();
  const keywordKodierteSonderzeichen = encodeURI(keyword);
  let art = 'Keyword';

  $.get(`http://127.0.0.1:3000/api/keyword/${keywordKodierteSonderzeichen}`, function (tabellenEintraege, status) {
    nachOrtFuellen(tabellenEintraege, status, art);
  });

  filterEntfernenButton();
}

function nachFavouriteFiltern(ort) {
  const ortKodierteSonderzeichen = encodeURI(ort);
  let art = 'Favourite';

  $.get(`http://127.0.0.1:3000/api/ort/${ortKodierteSonderzeichen}`, function (tabellenEintraege, status) {
    nachOrtFuellen(tabellenEintraege, status, art);
  });

  filterEntfernenButton();
}

function nachOrtFuellen(data, status, art) {
  if (status == 'success') {
    $('table').empty();

    if (data[0]) {
      //Prueft, ob mindestens ein entsprechender Eintrag vorhanden ist

      $('#fehlerKeinOrt').addClass('is-hidden');
      $('#fehlerKeinOrt').addClass('is-invisible');
      $('#fehlerKeinKeyword').addClass('is-hidden');
      $('#fehlerKeinKeyword').addClass('is-invisible');

      let table = document.querySelector("table");
      let keys = Object.keys(data[0]);
      generateTable(table, data);
      generateTableHead(table, keys);
      addEventListenerAnzeigen();
    }
    else {
      if (art === 'Ort') {
        $('#fehlerKeinOrt').removeClass('is-hidden');
        $('#fehlerKeinOrt').removeClass('is-invisible');
      } else if (art === 'Keyword') {
        $('#fehlerKeinKeyword').removeClass('is-hidden');
        $('#fehlerKeinKeyword').removeClass('is-invisible');
      }
    }
  }
}

function filterEntfernenButton() {
  $('#buttonFilterEntfernen').removeClass('is-hidden');
  $('#buttonFilterEntfernen').removeClass('is-invisible');
}