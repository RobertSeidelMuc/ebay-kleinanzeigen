$( document ).ready(() => {
    $('#linkAnzeigeErstellen').click(viewAnzeigeErstellen);
    // $('.linkAktuelleAnzeigen').click(viewAktuelleAnzeigen);
    // $('#linkSuche').click(viewSuche); // Thomas 20200504: Entfallen.
    $(".navbar-burger").click(navBarBurgerMenu);
    
    viewAktuelleAnzeigen();
});

function navBarBurgerMenu() {
    // Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
    $(".navbar-burger").toggleClass("is-active");
    $(".navbar-menu").toggleClass("is-active");
}

function viewAnzeigeErstellen()
{
    $('body').off('click', '.delete');
    $('body').off('click', '.linkAktuelleAnzeigen');

    $('#seiteninhalt').load('anzeigeErstellen.html', function () {
        $('#absenden').click(anzeigePruefen);
        $('.linkAktuelleAnzeigen').click(viewAktuelleAnzeigen); // Thomas 20200504: Wird schon beim Laden der Seite gesetzt, wuerde sich ansonsten vervielfaeltigen, wenn hier nochmal.
        $('.delete').click(closeError);
    });
}


function viewAktuelleAnzeigen() {
    // $('#seiteninhalt').load('anzeigeliste.html'); // Thomas 20200504: Wird in clientcode.js aufgerufen, daher hier ueberfluessig.
    viewAnzeigeListe();
}

// function viewSuche() {
//     $('#seiteninhalt').load('suche.html');
// }

function anzeigePruefen() {
    if (!$('#vorname').val()
        || !$('#nachname').val()
        || !$('#ort').val()
        || !$('#titel').val()
        || !$('#beschreibung').val()
        || !$('#kontakt').val()) {
        $('#error').removeClass('is-hidden');
        $('#error').removeClass('is-invisible');
        $('#leerePflichtfelder').removeClass('is-hidden');
        $('#leerePflichtfelder').removeClass('is-invisible');
    } else { 
        if($('#preis').val()<0) {
            $('#error').removeClass('is-hidden');
            $('#error').removeClass('is-invisible');
            $('#negativerPreis').removeClass('is-hidden');
            $('#negativerPreis').removeClass('is-invisible');
        }
        else { 
            anzeigeAbsenden();
            $('#sended').removeClass('is-hidden');
            $('#sended').removeClass('is-invisible')
            $('#error').addClass('is-hidden');
            $('#error').addClass('is-invisible');
            $('#leerePflichtfelder').addClass('is-hidden');
            $('#leerePflichtfelder').addClass('is-invisible');
            $('#negativerPreis').addClass('is-hidden');
            $('#negativerPreis').addClass('is-invisible');
        }
    }
}

function closeError() {
    $('#error').addClass('is-hidden');
    $('#error').addClass('is-invisible');
    $('#sended').addClass('is-hidden');
    $('#sended').addClass('is-invisible');
}

function anzeigeAbsenden() {
    let neueAnzeige = {};


    neueAnzeige.vorname = $('#vorname').val();
    neueAnzeige.nachname = $('#nachname').val();
    neueAnzeige.ort = $('#ort').val();
    neueAnzeige.titel = $('#titel').val();
    neueAnzeige.beschreibung = $('#beschreibung').val();
    if($('#preis').val()===""){
        neueAnzeige.preis=0
    }
    else{
        neueAnzeige.preis = $('#preis').val();
    }
    neueAnzeige.verhandlungsbasis = document.getElementById('verhandlungsbasis').checked ? 1 : 0;
    neueAnzeige.kontakt = $('#kontakt').val();

    const neueAnzeigeJSON = JSON.stringify(neueAnzeige);


    $.ajax({
        type: 'POST',
        url: 'http://127.0.0.1:3000/api/entry',
        contentType: 'application/json',
        crossDomain: true,
        data: neueAnzeigeJSON,
        dataType: 'json',
        success: console.log(`Neue Anzeige an den Server übermittelt: ${neueAnzeigeJSON}`)
    });
}